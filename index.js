    const WebSocket = require('ws');
    const http = require('http');
    const url = require('url');


    http.createServer(function (request, response) {
        const ws = new WebSocket('wss://zijian.aliyun.com/detect');
        ws.on('open', function open() {
            let obj = url.parse(request.url, true);
            console.log(obj.query.host);
            var send_data = '{"type":"search","data":"' + obj.query.host + '"}';
            ws.send(send_data);
        });

        var send_arr = [];
        response.writeHead(200, { "Content-Type": "text/html;charset=utf-8" });
        ws.onmessage = function (e) {
            var js_obj = JSON.parse(e.data);
            send_arr.push(js_obj)
            if (js_obj.action == "CLOSE") {
                var msg = JSON.stringify(send_arr);
                response.write(msg);
                response.end();
            }
        };

    }).listen(process.env.PORT||8005);
    console.log('Server running at http://0.0.0.0:/'+process.env.PORT||8005);